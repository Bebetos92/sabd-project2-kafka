package com.sabd.project2.output;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OutputQueries {

    @Expose
    @SerializedName("type")
    private String type;

    @Expose
    @SerializedName("value")
    private String value;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
