package com.sabd.project2.sender.workers;

import com.sabd.project2.Global;
import com.sabd.project2.sender.workers.Kafka.KafkaSender;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Objects;

public class WorkerComments implements Runnable {
    @Override
    public void run() {

        try {
            KafkaSender sender = new KafkaSender();
            ClassLoader resourceLoader = getClass().getClassLoader();
            File myFile = new File(Objects.requireNonNull(resourceLoader.getResource(Global.FILENAME_COMMENTS)).getFile());
            BufferedReader reader = new BufferedReader(new FileReader(myFile));

            String line = reader.readLine();
            while (true){
                String line2 = reader.readLine();
                if(line==null){
                    break;
                }
                sender.sendMessageToTopic(Global.TOPIC_COMMENTS,line,Global.BROKER_COMMENT);
                if(line2==null){
                    break;
                }
                //long sleep = Tools.getLongDate(line2.substring(0, line2.indexOf("|")))-Tools.getLongDate(line.substring(0, line.indexOf("|")));
                //Thread.sleep(sleep);
                line=line2;
            }
            System.out.println("Done comments!");

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
