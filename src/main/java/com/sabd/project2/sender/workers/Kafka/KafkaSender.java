package com.sabd.project2.sender.workers.Kafka;

import com.sabd.project2.Global;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Properties;

//Create java class named “kafka.producer.SimpleProducer”
public class KafkaSender {

    public KafkaSender(){
        super();
    }

    private Properties createProperties(String broker){

        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,broker);

        //Set acknowledgements for producer requests.
        props.put("acks", "all");
        //If the request fails, the producer can automatically retry,
        props.put("retries", 0);
        //Specify buffer size in config
        props.put("batch.size", 16384);
        //Reduce the no of requests less than 0
        props.put("linger.ms", 1);
        //The buffer.memory controls the total amount of memory available to the producer for buffering.
        props.put("buffer.memory",33554432);
        props.put("key.serializer",
                "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer",
                "org.apache.kafka.common.serialization.StringSerializer");
        return props;
    }

    private Producer<String, String> getProducer(String broker) {
        return new KafkaProducer<>(createProperties(broker));
    }

    public void sendMessageToTopic(String topicName, String value,String broker){

        Producer<String,String> producer = getProducer(broker);
        producer.send(new ProducerRecord<>(topicName,value));
        producer.close();
    }
}