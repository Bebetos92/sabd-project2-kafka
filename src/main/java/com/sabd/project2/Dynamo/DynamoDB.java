package com.sabd.project2.Dynamo;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.sabd.project2.Global;
import java.util.HashMap;
import java.util.Map;

public class DynamoDB {

    private static BasicAWSCredentials awsCreds = new BasicAWSCredentials(Global.AWS_ACCESS_KEY, Global.AWS_SECRET_KEY);
    private AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard()
            .withCredentials(new AWSStaticCredentialsProvider(awsCreds)).withRegion("eu-west-1")
            .build();

    private static DynamoDB instance = null;

    public static DynamoDB getInstance() {
        if (instance == null) {
            instance = new DynamoDB();
        }
        return instance;
    }


    //Torna true in caso di put avvenuta con successo, altrimenti torna false
    public boolean putResult(String table, String keyValue, String value,String type) {
        HashMap<String, AttributeValue> item_values =
                new HashMap<>();

        item_values.put(Global.DYNAMODB_KEY, new AttributeValue(keyValue+"-"+type));
        item_values.put(Global.DYNAMODB_VALUE, new AttributeValue(value));

        try {
            client.putItem(table, item_values);
        } catch (AmazonServiceException e) {
            return false;
        }
        return true;
    }

    //Ritorna il valore assegnato all'ID del semaforo, in caso non ci fosse torna -1
    public String getResult(String table, String keyValue,String type) {

        HashMap<String, AttributeValue> key_to_get =
                new HashMap<>();

        key_to_get.put(Global.DYNAMODB_KEY, new AttributeValue(keyValue+"-"+type));
        GetItemRequest request = new GetItemRequest()
                .withKey(key_to_get)
                .withTableName(table);
        try {
            Map<String, AttributeValue> returned_item =
                    client.getItem(request).getItem();
            if (returned_item != null) {
                return returned_item.get(Global.DYNAMODB_VALUE).getS();
            } else {
                return null;

            }
        } catch (AmazonServiceException e) {
            return null;
        }
    }


    /*public static void main(String[] args) {
        if(DynamoDB.getInstance().putResult(Global.DYNAMODB_TABLE,"Query1","CIAO","type1")){
            System.out.println("SUCCESS");
        }
        System.out.println(DynamoDB.getInstance().getResult(Global.DYNAMODB_TABLE,"Query1","type1"));
    }*/


}
