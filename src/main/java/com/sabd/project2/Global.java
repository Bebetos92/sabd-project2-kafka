package com.sabd.project2;

public class Global {

    //Kafka Configuration
    public static String BROKER_FRIENDSHIP ="localhost:9092";
    public static String BROKER_COMMENT ="localhost:9093";
    public static String BROKER_POST ="localhost:9094";
    public static String ZOOKEEPER = "localhost:2181";

    //Topic Input to Flink
    public static String TOPIC_COMMENTS = "topicComments";
    public static String TOPIC_FRIENDSHIP = "topicFriendship";
    public static String TOPIC_POSTS = "topicPosts";

    //Topic consumer Output from Flink
    public static String TOPIC_QUERY1 = "topicQuery1";
    public static String TOPIC_QUERY2 = "topicQuery2";
    public static String TOPIC_QUERY3 = "topicQuery3";

    //FileName stream
    public static String FILENAME_COMMENTS = "comments.dat";
    public static String FILENAME_FRIENDSHIP = "friendships.dat";
    public static String FILENAME_POSTS = "posts.dat";

    //DynamoDB config
    public static String DYNAMODB_TABLE = "sabd-prj2";
    public static String DYNAMODB_KEY = "query";
    public static String DYNAMODB_VALUE = "value";

    //AWS credentials
    public static String AWS_ACCESS_KEY = "insert your accessKey";
    public static String AWS_SECRET_KEY = "insert your secretKey";

}
