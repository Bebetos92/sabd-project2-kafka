package com.sabd.project2.receiver.workers;

import com.google.gson.Gson;
import com.sabd.project2.Dynamo.DynamoDB;
import com.sabd.project2.Global;
import com.sabd.project2.output.OutputQueries;
import com.sabd.project2.receiver.workers.Kafka.KafkaConsumer;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecords;

public class ConsumerQuery2 implements Runnable {
    @Override
    public void run() {
        Consumer<String, String> consumer = KafkaConsumer.createConsumer(Global.TOPIC_QUERY2, Global.BROKER_COMMENT);

        Gson gson = new Gson();

        //Consumer che scrive su DynamoDB le tuple risultato. Verranno usate le key dei topic di Kafka per definire l'intervallo della query
        while (true) {
            ConsumerRecords<String, String> consumerRecords = consumer.poll(1000);

            consumerRecords.forEach(record -> {

                OutputQueries out = gson.fromJson(record.value(), OutputQueries.class);
                DynamoDB.getInstance().putResult(Global.DYNAMODB_TABLE, "query2", out.getValue(), out.getType());

            });

            //Alternativa sincrona
            consumer.commitAsync();
        }
    }
}
