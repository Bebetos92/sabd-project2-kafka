package com.sabd.project2;

import com.sabd.project2.receiver.workers.ConsumerQuery1;
import com.sabd.project2.receiver.workers.ConsumerQuery2;
import com.sabd.project2.receiver.workers.ConsumerQuery3;
import com.sabd.project2.sender.workers.WorkerComments;
import com.sabd.project2.sender.workers.WorkerFriendship;
import com.sabd.project2.sender.workers.WorkerPosts;

public class Start {

    public static void main(String[] args) {

        Thread threadComments = new Thread(new WorkerComments());
        threadComments.start();
        Thread receiveComments = new Thread(new ConsumerQuery2());
        receiveComments.start();

        Thread threadFriendship = new Thread(new WorkerFriendship());
        threadFriendship.start();
        Thread receiveFriendship = new Thread(new ConsumerQuery1());
        receiveFriendship.start();

        Thread threadPosts = new Thread(new WorkerPosts());
        threadPosts.start();
        Thread receivePosts = new Thread(new ConsumerQuery3());
        receivePosts.start();
    }
}
