# SABD Project2 Kafka #

## Introduzione ##

La seguente repository gestisce:  

 * l'invio dei dati contenuti nei file al cluster Flink: [vedi repository](https://bitbucket.org/Andreatop90/sabd-project2-flink/src/master/)  
 * la scrittura dei dati processati su DynamoDB

## Configurazione ##

Prima di avviare il progetto è necessario impostare le chiavi AWS per accedere al servizio di dynamoDB.
Le variabili sono salvate all'interno della classe Global.

```java
//AWS credentials
public static String AWS_ACCESS_KEY = "insert your accessKey"; 
public static String AWS_SECRET_KEY = "insert your secretKey";
```

### ATTENZIONE ###

Scritture eccessive possono portare al ban da parte di Amazon!


## Developer ##

* Cenciarelli Andrea
* Talone Alberto